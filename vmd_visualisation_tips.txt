Note that because LAMMPS does not output the bead positions in order (i.e., by
the bead index) while VMD thinks that is the case, beads are linked together
wrongly in the visualisation. We can fix this by typing the following set of
commands in the terminal after loading the molecule:

      topo clearbonds
      set N {nbeads}
      for {set i 0} {$i < [expr $N-1]} {incr i} {
      	  set j [expr $i+1]
	  topo addbond $i $j
      }

[You will need to replace the variable nbeads with the actual number of beads
in the polymer]
      
