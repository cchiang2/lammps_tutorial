# Tutorial 0 -- simulating a Lennard-Jones fluid

In this first tutorial, we use LAMMPS to perform molecular dynamics (MD)
simulations of a Lennard-Jones (LJ) fluid (colloidal particles in a solvent).
In the simulation we have two types of particles (atoms) situated in a
periodic box, and they interact with each other via a truncated and shifted LJ
potential, which is given by

   U_LJ/cut(r) = U_LJ(r)-U_LJ(rc) if r < rc

and 0 otherwise. Here, r is the distance between two particles, rc is the
cutoff distance at which point the potential is shifted and truncated, and
U_LJ is the usual Lennard-Jones potential

   U_LJ(r) = 4*epsilon*((sigma/r)^12-(sigma/r)^6),

with epsilon being the interaction strength and sigma the distance where U = 0
(the bead size).

Rather than simulating all the solvent particles (which is computationally
very expensive!), we model their effect on the colloidal particles implicitly
by including more terms to the equations of motion, in addition to conservative
forces (F_con), such that

   m*dv(t)/dt = F_con + F_damp + F_rand

with

   F_con = -nabla(U)
   F_damp = -gamma*v(t)
   F_rand = sqrt(2.0*gamma*k_B*T)*eta(t).

Here, F_damp is a damping force on the particle (with gamma being the damping
const and v the particle's velocity), and F_rand is a random force on the
particle due to collisions with the solvent particles. eta(t) is a random
'noise' vector that obeys the following statistical averages

   <eta(t)> = 0
   <eta_i(t)eta_j(t')> = delta(t-t')delta_ij

where the first delta in the second equation is a Dirac delta and the second
one is a Kronecker delta, and i,j are indices running over Cartesian
components. This type of equations is known as Langevin equations, and you
will learn more about them in the Year 4 Statistical Physics course. Often MD
simulations using this scheme are referred as 'Brownian/Langevin dynamics'
simulations.

###############################################################################

What to do:

0. Familiarise yourself with the LJ potential

   Using your favourite plotting package, plot the truncated and shifted LJ
   potential for different parameter values (epsilon and rc) and have a think
   about the following questions:
   -  At what critical value for rc does the potential become purely
      repulsive? Note that setting rc to this value (with epsilon = 1.0)
      gives a potential also known as the Weeks-Chandler-Andersen (WCA)
      potential. [Hint: have a look at the provided bash script init.sh if you
      are unsure.]
   -  How does shifting the LJ potential change the value of its global
      minimum? How does this depend on rc? Is there a way we can ensure that
      the minimum remains the same as that for the non-truncated LJ potential
      (i.e., that the minimum reaches -epsilon)?
	 
1. Run the simulation to get a feel of how to use LAMMPS

   a. Generate the required input scripts/files

      This is done by running the provided bash script

         bash init.sh

      which creates two files that are needed for running LAMMPS:
      -  run.lam file - a 'driver' script that is read by LAMMPS
      -  init.in file - a trajectory file containing the initial configuration
                        of the system (i.e., the atoms' positions, bonds, etc)

      Have a look at these files individually and make sure you understand
      what each function and each section does!

   b. Run the simulation

      You can use the pre-compiled version of LAMMPS available on the school
      Linux computers. Alternatively, you can also compile LAMMPS on your
      own computer (e.g., using the script provided in lammps/install.sh or
      following the instructions on docs.lammps.org/Install.html to install
      LAMMPS). To start the simulation, we use the command

         lmp -in run.lam

      (You will need to replace lmp with the directory to the LAMMPS executable
      if you have installed your own version of LAMMPS - e.g.,
      lammps/programs/lmp_serial)

      The simulation generates several files:
      -  pos*.lammpstrj - these files contain the trajectory of the atoms
                          (for equilibration and the main simulation)
      -  log.lammps     - this contains similar info to what has been output
      	 		  to the screen (e.g., thermodynamic info, computation
			  time, etc.)
      See the comments next to the 'dump' command in the run.lam to understand
      the file format for the trajectory files.

   c. Visualise the atoms' trajectories

      This can be done using VMD on the school computers. You can also
      download VMD using this link:

      	 www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=VMD
      
      You can visualise the trajectory file as follows:

      -  Type 'vmd' in the terminal
      -  In the VMD Main window, click on 'File' -> 'New Molecule...'. In the
      	 pop-up window, click on 'Browse...' and select the trajectory file
	 (pos.lammpstrj). Click 'Load' to load the data
      -  The display will load the unwrapped positions of the atoms. To
      	 'rewrap' their positions, we need to type the following in the
	 terminal:
	 
	 pbc set {lx ly lz} -all
	 pbc wrap -center origin -all

	 [You will need to replace lx ly lz with the actual box size]
	
      -  To get a nicer representation, you can make the following changes. In
      	 the VMD Main window, click on 'Graphics' -> 'Representations...'. In
	 the pop-up window, select 'VDW' as the Drawing Method and use
	 'AOChalky' for the Material. Feel free to play around with other
	 display settings!

2. Play around with the simulation parameters

   Explore how changing the number (density) of atoms and the interaction
   strength between different types of atoms affects the overall dynamics of
   the system. This can be done by editing the parameter values in the init.sh
   script, in particular

   natoms     = number of atoms in total
   epsilon_ab = the interaction strength between type a and type b atoms
   rc_ab      = the cutoff distance of the potential for the interaction
                between type a and type b atoms

   See if you can find a range of parameter values where
   -  the two types of atoms are well mixed
   -  the two types of atoms form their own clusters

   For a more interesting challenge, try modifying init.sh to simulate a
   system with 3 (or more) types of atoms!

3. Understand the units used in the simulations

   Simulations are often conducted in 'reduced' units to maintain generality
   of the results and to limit errors introduced from floating-point
   arithmetic caused by doing computation on variables with values spanning
   many different orders of magnitude. This is achieved by rescaling variables/
   observables by a set of fundamental quantities characteristic to the
   system. In the case of an LJ fluid, three key properties are the energy,
   length, and mass of the system, and a natural choice is to rescale these by
   the thermal energy (k_B*T), the size of a colloidal particle (sigma), and
   its mass (m). In other words, we have k_B*T = 1.0, sigma = 1.0 and m = 1.0
   in reduced/simulation units (also known as LJ units for this particular
   rescaling in LAMMPS).
   -  Based on these fundamental units, can you construct a natural time
      scale to describe the system? [Hint: take a look at the 'units' command
      in LAMMPS documentation.]
   -  A more challenging task - looking at the dimensions of the variables
      involved in the equations of motion above, can you identify other
      relevant time scales for describing the system?
