#!/bin/bash
# init.sh
# A script to generate the LAMMPS script and init config file

# Make sure we are using python3
pyx=python3

# Set the box size, number of atoms and seed for the random generator
lx=50
ly=50
lz=50
natoms=1000
ntypes=2
seed=3432

# Set LAMMPS parameters
# LJ potential
# Try changing epsilon and cutoff (rc) and see how that affects the dynamics!
epsilon_11=1.0
epsilon_12=1.0
epsilon_22=1.0
rc_11=1.8
rc_12=1.8
rc_22=1.8
rc_0=1.122462048309373 # 2^(1/6) - the cutoff distance for a purely
                       # repulsive (WCA) potential

sigma=1.0 # Set all atoms to have the same size

# Rescale epsilon such that the minimum of the truncated and shifted LJ
# potential actually reaches -epsilon
function rescale_energy() {
    local e=$1
    local rc=$2
    local s=$3
    echo $($pyx -c "
norm = 1.0+4.0*(($s/$rc)**12.0-($s/$rc)**6.0)
print($e/norm if norm > 0.0 else $e)")
}
epsilon_11=$(rescale_energy $epsilon_11 $rc_11 $sigma)
epsilon_12=$(rescale_energy $epsilon_12 $rc_12 $sigma)
epsilon_22=$(rescale_energy $epsilon_22 $rc_22 $sigma)

# Simulation run times (in simulation time unit)
dt=0.01 # Size of each timestep
run_init_time=100    # Equilibration run time
run_time=10000       # Main simulation run time

# Dump frequencies (in simulation time unit)
dump_printfreq=100   # Frequency for dumping atom positions
thermo_printfreq=100 # Frequency for dumping thermodynamics info

# Convert run times and dump frequencies to be in timesteps
function get_timestep() {
    local tau=$1
    local dt=$2
    echo $($pyx -c "print(int($tau/$dt))")
}
run_init_time=$(get_timestep $run_init_time $dt)
run_time=$(get_timestep $run_time $dt)
dump_printfreq=$(get_timestep $dump_printfreq $dt)
thermo_printfreq=$(get_timestep $thermo_printfreq $dt)

# Set the box boundaries
xlo=$($pyx -c "print(-int(${lx}/2.0))")
xhi=$($pyx -c "print(int(${lx}/2.0))")
ylo=$($pyx -c "print(-int(${ly}/2.0))")
yhi=$($pyx -c "print(int(${ly}/2.0))")
zlo=$($pyx -c "print(-int(${lz}/2.0))")
zhi=$($pyx -c "print(int(${lz}/2.0))")

# Output files
pos_equil_file="pos_equil.lammpstrj"
pos_file="pos.lammpstrj"

# Generate the LAMMPS input file
atom_py="create_atoms.py"
traj_in="traj.in"
$pyx $atom_py $natoms $ntypes $lx $ly $lz $seed $traj_in

init_file="init.in"
echo "LAMMPS data file via

${natoms} atoms
${ntypes} atom types

${xlo} ${xhi} xlo xhi
${ylo} ${yhi} ylo yhi
${zlo} ${zhi} zlo zhi
" > $init_file

awk -v nt=$ntypes 'BEGIN {
print "Masses"
print ""
for (i = 1; i <= nt; i++) {
print i,1
}
print ""
print "Atoms"
print ""
}{
print NR,$2,$3,$4,$5,0,0,0
}' $traj_in >> $init_file
rm $traj_in

# Generate the script to run LAMMPS
lam_file="run.lam"
echo "
##################################################

# Simulation basic setup

units lj # Set simulation unit - for LJ, distance = sigma, energy = k_B*T
atom_style atomic # State the system is only made of atoms without bonds/angles
boundary p p p # Set periodic boundary conditions

neighbor 1.9 bin # Set how LAMMPS creates neighbour lists
neigh_modify every 1 delay 1 check yes # How often neighbour lists are created

# Read the file with atoms' initial positions
read_data $(basename $init_file)

##################################################

# Dumps

# Set the params for dumping thermodynamic properties of the system to the
# screen and/or log file
thermo ${thermo_printfreq} # Set the output frequency
# Set the specific observables to dump (see LAMMPS manual for more details)
thermo_style custom step temp epair vol 

# Set up a dump to output atom positions
# xs,ys,zs - positions normalised by the box size (so between 0.0 and 1.0)
# ix,iy,iz - number of times the atoms crossed each periodic boundary
dump 1 all custom ${dump_printfreq} $(basename $pos_equil_file) &
id type xs ys zs ix iy iz

##################################################

# Potentials

# Use the repulsive soft potential for equilibration to push apart atoms that 
# are close together and would cause numerical divergence had one applied the
# LJ potential directly
# U(r) = A*(1+cos(pi*r/rc))
pair_style soft ${rc_0}

# Set the params for the pairwise interactions between individual atom types
# Key params: atom_type_1 atom_type_2 A rc
pair_coeff * * 100.0 ${rc_0} # This sets all pairwise interactions

##################################################

# Set integrator/dynamics

# Use the NVE ensemble (particle number, volume, and energy conserved)
fix 1 all nve
# Use langevin thermostat for Brownian dynamics
# Key params: T_start T_stop damp seed
fix 2 all langevin 1.0 1.0 1.0 ${seed} 

##################################################

# Initial equilibration

# Set the timestep and run the simulation
timestep ${dt}
run ${run_init_time}

##################################################

# Main simulation

# Use a truncated and shifted LJ potential for the interactions between atoms
# U_LJ/cut(r) = U_LJ(r)-U_LJ(rc) if r < rc and 0 otherwise, where
# U_LJ(r) = 4.0*epsilon*((sigma/r)^12-(sigma/r)^6)
# and rc is the location of the cutoff
# If rc = 2^(1/6) ~ 1.12246, this becomes a purely repulsive (WCA) potential
# If rc > 2^(1/6), then there is a region where the potential is attractive
pair_style lj/cut ${rc_0}

# Make sure the LJ potential is shifted by the energy value at the cutoff.
# This only changes the pairwise energy values output by LAMMPS but has no
# effect on the actual dynamics
pair_modify shift yes

# Set the params for the pairwise interactions between individual atom types
# Key params: atom_type_1 atom_type_2 epsilon sigma rc
# Try changing epsilon and rc (cutoff) and see how that affects the dynamics!
pair_coeff 1 1 ${epsilon_11} ${sigma} ${rc_11}
pair_coeff 1 2 ${epsilon_12} ${sigma} ${rc_12}
pair_coeff 2 2 ${epsilon_22} ${sigma} ${rc_22}

##################################################

# Dumps

undump 1 # Unset previous dump settings
dump 1 all custom ${dump_printfreq} $(basename $pos_file) &
id type xs ys zs ix iy iz

##################################################

# Reset time and run the main simulation
reset_timestep 0
run ${run_time}
" > $lam_file
