#!/usr/bin/env python3
# create_atoms.py
# A simple script to generate atoms in a box of size (lx,ly,lz). The centre of
# the box is at the origin.

import sys
import numpy as np

args = sys.argv

if (len(args) != 8):
    print("Usage: create_atoms.py natoms ntypes lx ly lz seed out_file")
    sys.exit(1)

natoms = int(args.pop(1)) # Number of atoms
ntypes = int(args.pop(1)) # Number of atom types
lx = float(args.pop(1))   # Box dimension in the x direction
ly = float(args.pop(1))   # Box dimension in the y direction
lz = float(args.pop(1))   # Box dimension in the z direction
seed = int(args.pop(1))   # Seed for the random generator
out_file = args.pop(1)    # Name of the output file

# Set up the random generator
rng = np.random.default_rng(seed)

xhalf = lx/2.0
yhalf = ly/2.0
zhalf = lz/2.0

# Generate and output the atom positions
with open(out_file,'w') as writer:
    for i in range(1,natoms+1):
        t = rng.integers(1,ntypes+1)
        x = rng.random()*lx-xhalf
        y = rng.random()*ly-yhalf
        z = rng.random()*lz-zhalf
        writer.write("{:d} {:d} {:f} {:f} {:f}\n".format(i,t,x,y,z))
