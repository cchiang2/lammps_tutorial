#!/bin/bash
# init.sh
# A script to generate the LAMMPS script and init config file

# Make sure we are using python3
pyx=python3

# Set the box size, number of DNA/chromatin beads, number of proteins,
# and the seed for the random generator
lx=50
ly=50
lz=50
nbeads=1000
nprots=100
ntypes=2 # Number of bead types = 2 (DNA/chromatin and protein types)
seed=51236

# Set LAMMPS parameters
# LJ potential
epsilon=3.0 # Interaction strength between DNA/chromatin and protein beads
rc=1.8
rc_0=1.122462048309373 # 2^(1/6) - the cutoff distance for a purely
                       # repulsive (WCA) potential

sigma=1.0 # Set all chromatin and protein beads to have the same size

# FENE potential
K_f=30.0
R_0=1.6

# Angle potential
l_p=3.0 # Set the persistence length of the DNA/chromatin fibre

# Rescale epsilon such that the minimum of the truncated and shifted LJ
# potential actually reaches -epsilon
function rescale_energy() {
    local e=$1
    local rc=$2
    local s=$3
    echo $($pyx -c "
norm = 1.0+4.0*(($s/$rc)**12.0-($s/$rc)**6.0)
print($e/norm if norm > 0.0 else $e)")
}
epsilon=$(rescale_energy $epsilon $rc $sigma)

# Simulation run times (in simulation time unit)
dt=0.01 # Size of each timestep
run_init_time_1=100  # Equilibration time with harmonic bonds
run_init_time_2=100  # Equilibration time with FENE bonds
run_time=10000       # Main simulation run time

# Dump frequencies (in simulation time unit)
dump_printfreq=100   # Frequency for dumping bead positions
thermo_printfreq=100 # Frequency for dumping thermodynamics info

# Convert run times and dump frequencies to be in timesteps
function get_timestep() {
    local tau=$1
    local dt=$2
    echo $($pyx -c "print(int($tau/$dt))")
}
run_init_time_1=$(get_timestep $run_init_time_1 $dt)
run_init_time_2=$(get_timestep $run_init_time_2 $dt)
run_time=$(get_timestep $run_time $dt)
dump_printfreq=$(get_timestep $dump_printfreq $dt)
thermo_printfreq=$(get_timestep $thermo_printfreq $dt)

# Set the box boundaries
xlo=$($pyx -c "print(-int(${lx}/2.0))")
xhi=$($pyx -c "print(int(${lx}/2.0))")
ylo=$($pyx -c "print(-int(${ly}/2.0))")
yhi=$($pyx -c "print(int(${ly}/2.0))")
zlo=$($pyx -c "print(-int(${lz}/2.0))")
zhi=$($pyx -c "print(int(${lz}/2.0))")

# Output files
pos_equil_file="pos_equil.lammpstrj"
pos_file="pos.lammpstrj"

# Generate the LAMMPS input file
dna_py="create_dna_protein.py"
traj_in="traj.in"
$pyx $dna_py $nbeads $nprots $sigma $lx $ly $lz $seed $traj_in

init_file="init.in"
echo "LAMMPS data file via

$((${nbeads}+${nprots})) atoms
2 atom types
$((${nbeads}-1)) bonds
1 bond types
$((${nbeads}-2)) angles
1 angle types

${xlo} ${xhi} xlo xhi
${ylo} ${yhi} ylo yhi
${zlo} ${zhi} zlo zhi
" > $init_file

awk -v nt=$ntypes -v nb=$nbeads 'BEGIN {
print "Masses"
print ""
for (i = 1; i <= nt; i++) {
print i,1
}
print ""
print "Atoms # angle"
print ""
}{
print NR,1,$2,$3,$4,$5,0,0,0
} END {
print ""
print "Bonds"
print ""
for (i = 1; i <= nb-1; i++) {
print i,1,i,i+1
}
print ""
print "Angles"
print ""
for (i = 1; i <= nb-2; i++) {
print i,1,i,i+1,i+2
}}' $traj_in >> $init_file
rm $traj_in

# Generate the script to run LAMMPS
lam_file="run.lam"
echo "
##################################################

# Simulation basic setup

units lj # Set simulation unit - for LJ, distance = sigma, energy = k_B*T
atom_style angle # State the system includes bonds and angles
boundary p p p # Set periodic boundary conditions

neighbor 1.9 bin # Set how LAMMPS creates neighbour lists
neigh_modify every 1 delay 1 check yes # How often neighbour lists are created

comm_style tiled
comm_modify mode single cutoff 4.0 vel yes

# Read the file with the beads' initial positions
read_data $(basename $init_file)

##################################################

# Groups

# Define two groups - one for polymer beads and one for all beads. This allows
# measurements to be done separately on the different groups

group all type 1 2
group poly type 1

##################################################

# Dumps

# Set the params for dumping thermodynamic properties
thermo ${thermo_printfreq}
compute gyr poly gyration
thermo_style custom step temp epair vol c_gyr

# Set up a dump to output bead positions
dump 1 all custom ${dump_printfreq} $(basename $pos_equil_file) &
id type xs ys zs ix iy iz

##################################################

# Potentials

# Use harmonic bonds to link between beads for initial equilibration
bond_style harmonic
bond_coeff 1 100.0 1.1

# Use cosine angle bonds to model a semi-flexible polymer
angle_style cosine
angle_coeff 1 10.0 # Make the fibre very stiff first to help remove overlap

# Use the soft potential for equilibration to push apart beads close together
pair_style soft ${rc_0}
pair_coeff * * 100.0 ${rc_0}

##################################################

# Set integrator/dynamics

# Use the NVE ensemble (particle number, volume, and energy conserved)
fix 1 all nve
# Use the langevin thermostat for performing Brownian dynamics
fix 2 all langevin 1.0 1.0 1.0 ${seed} 

##################################################

# Initial equilibration

# Set the timestep and run the simulation
timestep ${dt}
run ${run_init_time_1}

##################################################

# Equilibrate with FENE bonds

# Switch from harmonic to FENE bonds
bond_style fene
bond_coeff 1 ${K_f} ${R_0} 1.0 ${sigma}
special_bonds fene

# Change the polymer to the desired stiffness
angle_coeff 1 ${l_p}

# Switch from soft to the purely repulsive LJ (or WCA) potential
pair_style lj/cut ${rc_0}
pair_modify shift yes
pair_coeff * * 1.0 ${sigma} ${rc_0}

run ${run_init_time_2}

##################################################

# Main simulation

# Change the LJ interactions from purely repulsive to the desired strength
pair_style lj/cut ${rc_0}
# Make all pairwise interactions repulsive first
pair_coeff * * 1.0 ${sigma} ${rc_0}
# Make the interaction between DNA/chromatin and protein beads attractive
pair_coeff 1 2 ${epsilon} ${sigma} ${rc}

##################################################

# Dumps

undump 1 # Unset previous dump settings
dump 1 all custom ${dump_printfreq} $(basename $pos_file) &
id type xs ys zs ix iy iz

##################################################

# Reset time and run the main simulation
reset_timestep 0
run ${run_time}
" > $lam_file
