#!/bin/bash
# install.sh
# A script to install all the packages required in a conda environment for
# running LAMMPS simulations

# Directory of this script
sh_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Default settings
env="lammps"      # Name of the created conda environment
nproc=1           # Number of processes to use when installing LAMMPS
keep_lmp_build=0  # Whether or not to keep the cmake files after installation
out_dir="${sh_dir}/programs"
conda_dir="${out_dir}/miniconda3"

# Help and option menu
function usage() {
    echo "
Usage: install.sh [options]

A script to compile LAMMPS for running molecular dynamics simulations

Options include:

  -h/--help                                       Print this help/option menu

  -c/--conda-dir CONDA_DIR                  	  The directory to an existing
  	      					  Miniconda/Anaconda base
						  environment (this can be
						  found by running the command
						  ``conda info'' and looking
						  for the directory of the
						  ``base environment''. Using
						  this option avoids installing
						  Miniconda locally.

  -o/--out-dir OUT_DIR				  The directory where all the
  	       					  files/executables generated
						  by running this script will
						  be stored. [Default:
						  ./programs/]
						  
  -n/--env ENV					  Name of the Conda environment
  						  that will be created
						  [Default: lammps]

  -j/--nproc NPROC				  Number of processes to use
  						  when installing LAMMPS
  						  [Default: 1]

  --keep-lammps-build				  Keep the LAMMPS source files
  						  and all the intermediate
  						  files (object files etc.)
						  when compiling LAMMPS using
						  CMake
  "
  exit 1
}

# Read optional arguments
function check_args() {
    if (( $# != 2 )); then
	echo "ERROR: Option $1 requires an additional argument"
	exit 1
    fi
}
while (( $# > 0 )); do
    op=$1
    case $op in
	-h|--help)
	    usage; break ;;
	-c|--conda-dir)
	    shift;
	    check_args $op $1 ;
	    conda_dir=$1 ;;	
	-o|--out-dir)
	    shift;
	    check_args $op $1 ;	    
	    out_dir=$1 ;;
	-n|--env-name)
	    shift;
	    check_args $op $1 ;	    
	    env=$1 ;;
	-j|--nproc)
	    shift;
	    check_args $op $1 ;	    
	    nproc=$1 ;;
	--keep-lammps-build)
	    keep_lmp_build=1 ;;
	--)
	    break ;;
	-* | --*)
	    echo "ERROR: Unknown option $1"; exit 1 ;;
    esac
    shift
done

# Check Conda and output directories exist
has_conda_dir=0
if [[ -d $conda_dir ]]; then
    has_conda_dir=1
else
    has_conda_dir=0
    conda_dir="${out_dir}/miniconda3"
    mkdir -p $conda_dir
fi
conda_dir=$(realpath $conda_dir)

mkdir -p $out_dir
out_dir=$(realpath $out_dir)

# Determine the OS type (only support Linux and MacOS)
if [[ $OSTYPE == "linux-gnu"* ]]; then
    is_macos=0
elif [[ $OSTYPE == "darwin"* ]]; then
    is_macos=1
else
    echo "ERROR: support for $OSTYPE is not available"
    exit 1
fi

# Key directories
src_dir="${sh_dir}/resources/src"

function setup_conda() {

if [[ $has_conda_dir == 0 ]]; then
    echo "Installing conda ..."
    if [[ $is_macos == 1 ]]; then
	curl https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-$(uname -m).sh -o ${conda_dir}/miniconda.sh    
    else
	curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-$(uname -m).sh -o ${conda_dir}/miniconda.sh
    fi
    bash ${conda_dir}/miniconda.sh -b -u -p $conda_dir
    rm ${conda_dir}/miniconda.sh
fi

}

# Create the conda environment
function create_conda_env() {

echo "Creating the conda environment ${env} ..."

pyx_version="3.10"

# Need this to activate a conda environment
source ${conda_dir}/etc/profile.d/conda.sh

if [[ $is_macos == 0 ]]; then
    conda create --name $env -c conda-forge -c bioconda \
	  python=$pyx_version gcc gxx gfortran cmake libjpeg-turbo \
	  libpng zlib gzip ffmpeg numpy
else
    conda create --name $env -c conda-forge -c bioconda \
	  python=$pyx_version clang clangxx gfortran llvm-openmp cmake \
	  libjpeg-turbo libpng zlib gzip ffmpeg numpy
fi

}

# Install LAMMPS
function install_lammps() {

echo "Installing LAMMPS ..."
lmp_tgz="${sh_dir}/resources/lammps-23Jun2022.tar.gz"
lmp_dir="${out_dir}/lammps-23Jun2022"
lmp_build_dir="${lmp_dir}/build"
lmp_src_dir="${lmp_dir}/src"

# Need this to activate a conda environment
source ${conda_dir}/etc/profile.d/conda.sh
conda activate $env

# Flags for compiling on macOS
if [[ $is_macos == 1 ]]; then
    compile_flags="-Xclang -fopenmp -I${CONDA_PREFIX}/include"
    link_flags="-L${CONDA_PREFIX}/lib/ -lomp -ljpeg -lpng -lz"
    omp_dylib="${CONDA_PREFIX}/lib/libomp.dylib"
else
    compile_flags="-fopenmp -I${CONDA_PREFIX}/include"
    link_flags="-L${CONDA_PREFIX}/lib/ -lgomp -ljpeg -lpng -lz"
fi

function extract() {
curl https://download.lammps.org/tars/lammps-23Jun2022.tar.gz -o ${sh_dir}/resources/lammps-23Jun2022.tar.gz
tar -xf $lmp_tgz -C ${out_dir}
cp ${src_dir}/bond_harmlj.* ${lmp_src_dir}/MOLECULE
}

function build() {

# Build the executable
mkdir -p $lmp_build_dir/exe
cd $lmp_build_dir/exe
if [[ $is_macos == 1 ]]; then
    cmake ../../cmake/ -D LAMMPS_MACHINE=serial -D BUILD_EXE=yes -D BUILD_MPI=no -D BUILD_OMP=yes -D BUILD_SHARED_LIBS=no -D BUILD_LIB=no -D PKG_MOLECULE=yes -D PKG_OPENMP=yes -D OpenMP_C_FLAGS="${compile_flags}" -D OpenMP_CXX_FLAGS="${compile_flags}" -D OpenMP_CXX_LIB_NAMES="libomp" -D OpenMP_libomp_LIBRARY="${omp_dylib}" -D CMAKE_SHARED_LINKER_FLAGS="${link_flags}"
else
    cmake ../../cmake/ -D LAMMPS_MACHINE=serial -D BUILD_EXE=yes -D BUILD_MPI=no -D BUILD_OMP=yes -D BUILD_SHARED_LIBS=no -D BUILD_LIB=no -D PKG_MOLECULE=yes -D PKG_OPENMP=yes -D CMAKE_C_FLAGS="${compile_flags}" -D CMAKE_CXX_FLAGS="${compile_flags}" -D CMAKE_SHARED_LINKER_FLAGS="${link_flags}" -D CMAKE_EXE_LINKER_FLAGS="${link_flags}"
fi
make -j $nproc
cp ${lmp_build_dir}/exe/lmp_serial $out_dir
chmod +x ${out_dir}/lmp_serial
if [[ $keep_lmp_build == 0 ]]; then
    rm -r ${lmp_build_dir}/exe
fi
       
# Build the shared library
mkdir -p ${lmp_build_dir}/share
cd ${lmp_build_dir}/share
if [[ $is_macos == 1 ]]; then
    cmake ../../cmake/ -D LAMMPS_MACHINE=serial -D BUILD_EXE=no -D BUILD_MPI=no -D BUILD_OMP=yes -D BUILD_SHARED_LIBS=yes -D BUILD_LIB=yes -D PKG_MOLECULE=yes -D PKG_OPENMP=yes -D OpenMP_C_FLAGS="${compile_flags}" -D OpenMP_CXX_FLAGS="${compile_flags}" -D OpenMP_CXX_LIB_NAMES="libomp" -D OpenMP_libomp_LIBRARY="${omp_dylib}" -D CMAKE_SHARED_LINKER_FLAGS="${link_flags}"
else
    cmake ../../cmake/ -D LAMMPS_MACHINE=serial -D BUILD_EXE=no -D BUILD_MPI=no -D BUILD_OMP=yes -D BUILD_SHARED_LIBS=yes -D BUILD_LIB=yes -D PKG_MOLECULE=yes -D PKG_OPENMP=yes -D CMAKE_C_FLAGS="${compile_flags}" -D CMAKE_CXX_FLAGS="${compile_flags}" -D CMAKE_SHARED_LINKER_FLAGS="${link_flags}" -D CMAKE_EXE_LINKER_FLAGS="${link_flags}"
fi
make -j $nproc
if [[ $is_macos == 1 ]]; then
    cp ${lmp_build_dir}/share/liblammps_serial.dylib ${out_dir}/liblammps.dylib
    chmod +x ${out_dir}/liblammps.dylib    
else
    cp ${lmp_build_dir}/share/liblammps_serial.so ${out_dir}/liblammps.so
    chmod +x ${out_dir}/liblammps.so    
fi
cp -r ${lmp_dir}/python $out_dir
if [[ $keep_lmp_build == 0 ]]; then
    rm -r ${lmp_build_dir}/share
    rm -r $lmp_dir
fi

cd $sh_dir
}

extract &&
build

}

setup_conda &&
create_conda_env &&
install_lammps
